// -----------
// Tue,  1 Mar
// -----------

/*
Class
	TTh, 8-10 am CT

Website
	https://www.oopl.com/ni/

Canvas
	two-stage quizzes
	4-mim individual
	4-min breakout room
	https://www.instructure.com/canvas/

GitLab: do a git clone, later git pull, on a regular basis
	notes
	examples
	exercises
	https://gitlab.com/gpdowning/oopl-ni.git

HackerRank: competitive programming platform
	exercises
	https://www.hackerrank.com/

Slack
	class questions
	ask and answer questions
	please be proactive
	https://oopl-ni-spring-2022.slack.com

Zoom
	5.9.6
	https://utexas.zoom.us/j/93131663569?pwd=MVQyK20zZmIrYTNzNFFVZlVQWlRndz09

Cold Calling
	you're only called ONCE per rotation
	it's totally fine to not know the answer, the idea is to discuss and to learn

Notes
	these notes will be posted on GitLab
*/

// ---------
// Hello.cpp
// ---------

// https://en.cppreference.com
// https://en.cppreference.com/w/cpp/io
// https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines.html

#include <iostream> // cout, endl

int main () {
    using namespace std;
    cout << "Nothing to be done." << endl;
    return 0;}

/*
Developed in 1978 by Bjarne Stroustrup of Denmark.
C++ is procedural, object-oriented, statically typed, and not garbage collected.

C++98
C++03
C++11
C++14
C++17
C++20
C++23



% which clang++
/usr/bin/clang++



% clang++ --version
Apple clang version 13.0.0 (clang-1300.0.29.30)
Target: x86_64-apple-darwin21.2.0
Thread model: posix
InstalledDir: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin



% clang++ -g -std=c++17 -Wall -Wextra -Wpedantic Hello.cpp -o Hello



% ./Hello
Nothing to be done.
*/

int i = 2;
int j = 3;
int k = (i << j);
cout << k;        // 16

i << j; // no, << has NO side effect, only has a return

(i << j) = k; // no,

/*
2 can be on the right hand side (RHS) of an assignment, but not the left
i can be on the right hand side (RHS) of an assignment, AND     the left
*/

2 = 5; // no
i = 2; // fine

/*
2 is an r-value
i is an l-value, stronger than an r-value
*/

/*
<<  on int, takes two r-values as arguments, and returns an r-value
<<= on int, takes an l-value and an r-value, and returns an l-value
*/

i <<= j; // yes, because it DOES have a side effect
i <<= 3;
2 <<= 3; // no

int k = (i <<= j); // <<= modifies i and returns i

(i <<= j) = k; // no in C, Java, Python; YES in C++

++(i <<= j);

cout << "Matt"; // output "Matt"

(cout << "hi ") << "Matt"; // output "hi Matt"

/*
type of cout? ostream

<< on ostream, takes an l-value and an r-value, and returns an l-value
*/

i <<= j <<= k; // i <<= (j <<= k);
(i <<= j) <<= k;

class Aaron {
	// overload <<
	// overload <<=
	}

/*
overload operators

associativity, can't change
precedence,    can't change
arity,         can't change

<< is NOT defined on float
can we define << on float? NO

<< is define on int
can we redefine << on int? NO

can we invent a token? NO

l-value/r-value nature of the args or the return, YES
*/

/*
unary operators
-
++
--
*/

/*
binary operators
+
*
/
%
<<
*/

/*
ternary operator
?:
*/

/*
n-ary operator
()
*/

/*
Collatz Conjecture, is about 100 years old

take a pos int
if even, divide   it by 2
if odd,  multiply it by 3 and add 1
repeat until 1
*/

1
5 16 8 4 2 1
10 5 16 8 4 2 1

/*
the cycle length of  5 is 6
the cycle length of 10 is 7
*/

/*
assertions
*/


// --------------
// Assertions.cpp
// --------------

// https://en.cppreference.com/w/cpp/error/assert

#include <cassert>  // assert
#include <iostream> // cout, endl

using namespace std;

int cycle_length (int n) {
    assert(n > 0); // using an assert for a pre-condition
    int c = 0;
    while (n > 1) {
        if ((n % 2) == 0) // if even
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0); // using an assert for a post-condition
    return c;}

void test () {
    assert(cycle_length( 1) == 1);  // using an assert to test my function
    assert(cycle_length( 5) == 6);
    assert(cycle_length(10) == 7);}

int main () {
    cout << "Assertions.cpp" << endl;
    test();
    cout << "Done." << endl;
    return 0;}

/*
% clang++ -g -std=c++17 -Wall -Wextra -Wpedantic Assertions.cpp -o Assertions
% ./Assertions
Assertions.cpp
Assertion failed: (c > 0), function cycle_length, file Assertions.cpp, line 21.



Turn OFF assertions at compile time with -DNDEBUG
% clang++ -g -std=c++17 -DNDEBUG -Wall -Wextra -Wpedantic Assertions.cpp -o Assertions
% ./Assertions
Assertions.cpp
Done.
*/

/*
assertions are good for:
	1. pre-conditions
	2. post-conditions
	3. loop invariants

	like computational comments!

assertions are NOT good for:
	1. testing,     what would be good is a unit test framework, like Google Test
	2. user errors, what would be good are exceptions
*/

// --------------
// UnitTests1.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h" // TEST, ASSERT_EQ

int cycle_length (int n) {
    assert(n > 0);
    int c = 0;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0); // built-in asserts
    return c;}

TEST(UnitTestsFixture, test0) {
    ASSERT_EQ(cycle_length( 1), 1);} // Google asserts

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 5), 6);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% g++-11 -pedantic -std=c++17 -O3 -Wall UnitTests1.cpp -o UnitTests1 -lgtest -lgtest_main -pthread



% ./UnitTests1
Running main() from /usr/local/src/googletest-master/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test0
Assertion failed: (c > 0), function cycle_length, file UnitTests1.cpp, line 22.
*/




// --------------
// UnitTests2.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test0) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 5), 5);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% ./UnitTests2
Running main() from /usr/local/src/googletest-master/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test0
[       OK ] UnitTestsFixture.test0 (0 ms)
[ RUN      ] UnitTestsFixture.test1
UnitTests2.cpp:29: Failure
Expected equality of these values:
  cycle_length( 5)
    Which is: 6
  5
[  FAILED  ] UnitTestsFixture.test1 (0 ms)
[ RUN      ] UnitTestsFixture.test2
[       OK ] UnitTestsFixture.test2 (0 ms)
[----------] 3 tests from UnitTestsFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test suite ran. (0 ms total)
[  PASSED  ] 2 tests.
[  FAILED  ] 1 test, listed below:
[  FAILED  ] UnitTestsFixture.test1

 1 FAILED TEST
*/





// --------------
// UnitTests3.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length( 5), 6);}

TEST(UnitTestsFixture, test3) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% ./UnitTests3
Running main() from /usr/local/src/googletest-master/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test1
[       OK ] UnitTestsFixture.test1 (0 ms)
[ RUN      ] UnitTestsFixture.test2
[       OK ] UnitTestsFixture.test2 (0 ms)
[ RUN      ] UnitTestsFixture.test3
[       OK ] UnitTestsFixture.test3 (0 ms)
[----------] 3 tests from UnitTestsFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test suite ran. (0 ms total)
[  PASSED  ] 3 tests.
*/


// -------------
// Coverage1.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(1), 1);}

/*
% g++-11 --coverage -pedantic -std=c++17 -O3 -Wall Coverage1.cpp -o Coverage1 -lgtest -lgtest_main -pthread



% ./Coverage1
Running main() from /usr/local/src/googletest-master/googletest/src/gtest_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.



% ls -al Coverage1.*
-rw-r--r--@ 1 downing  staff   1502 Sep  4 14:44 Coverage1.cpp
-rw-r--r--@ 1 downing  staff   3011 Sep  4 14:43 Coverage1.cpp.gcov
drwxr-xr-x@ 3 downing  staff     96 Sep  4 14:43 Coverage1.dSYM
-rw-r--r--@ 1 downing  staff   3712 Sep  4 14:43 Coverage1.gcda
-rw-r--r--@ 1 downing  staff  54816 Sep  4 14:43 Coverage1.gcno



% gcov-11 Coverage1.cpp | grep -B 2 "cpp.gcov"
File 'Coverage1.cpp'
Lines executed:66.67% of 12
Creating 'Coverage1.cpp.gcov'
*/



// -------------
// Coverage2.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(2), 2);}

/*
% ./Coverage2
Running main() from /usr/local/src/googletest-master/googletest/src/gtest_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.



% gcov-11 Coverage2.cpp | grep -B 2 "cpp.gcov"
File 'Coverage2.cpp'
Lines executed:91.67% of 12
Creating 'Coverage2.cpp.gcov'
*/




// -------------
// Coverage3.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(3), 8);}

/*
% ./Coverage3
Running main() from /usr/local/src/googletest-master/googletest/src/gtest_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.



% gcov-11 -b Coverage3.cpp | grep -B 2 "cpp.gcov"
File 'Coverage3.cpp'
Lines executed:100.00% of 12
Creating 'Coverage3.cpp.gcov'
*/
