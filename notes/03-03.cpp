// -----------
// Thu,  3 Mar
// -----------

/*
Class
	TTh, 8-10 am CT

Website
	https://www.oopl.com/ni/

Canvas
	two-stage quizzes
	4-mim individual
	4-min breakout room
	https://www.instructure.com/canvas/

GitLab: do a git clone, later git pull, on a regular basis
	notes
	examples
	exercises
	https://gitlab.com/gpdowning/oopl-ni.git

HackerRank: competitive programming platform
	exercises
	https://www.hackerrank.com/

Slack
	class questions
	ask and answer questions
	please be proactive
	https://oopl-ni-spring-2022.slack.com

Zoom
	5.9.6
	https://utexas.zoom.us/j/93131663569?pwd=MVQyK20zZmIrYTNzNFFVZlVQWlRndz09

Cold Calling
	you're only called ONCE per rotation
	it's totally fine to not know the answer, the idea is to discuss and to learn

Notes
	these notes will be posted on GitLab
*/

